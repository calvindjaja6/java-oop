package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal05 extends BasicLogic {
    public Logic01Soal05(int n){
        super(n);
    }

    public void isiArray(){
        int[] deret = new int[n];
        for (int i = 0; i < this.n; i++) {
            if (i <= 2) deret[i] = 1;
            else deret[i] = deret[i - 1] + deret[i - 2] + deret[i - 3];
            this.array[0][i] = String.valueOf(deret[i]);
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
