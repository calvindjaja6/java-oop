package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal07 extends BasicLogic {
    public Logic01Soal07(int n){
        super(n);
    }

    public void isiArray(){
        int angka = 65;
        for (int i = 0; i < this.n; i++) {
            this.array[0][i] = String.valueOf((char) angka);
            angka++;
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
