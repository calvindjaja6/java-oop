package Logic;

import Logic.Logic02.*;
import Logic.Logic01.*;

public class MainLogic {
    public static void main(String[] args) {
        int n = 9; //5,7,9,13
        Logic01Soal01 logic01Soal01 = new Logic01Soal01(n);
        Logic01Soal02 logic01Soal02 = new Logic01Soal02(n);
        Logic01Soal03 logic01Soal03 = new Logic01Soal03(n);
        Logic01Soal04 logic01Soal04 = new Logic01Soal04(n);
        Logic01Soal05 logic01Soal05 = new Logic01Soal05(n);
        Logic01Soal06 logic01Soal06 = new Logic01Soal06(n);
        Logic01Soal07 logic01Soal07 = new Logic01Soal07(n);
        Logic01Soal08 logic01Soal08 = new Logic01Soal08(n);
        Logic01Soal09 logic01Soal09 = new Logic01Soal09(n);
        Logic01Soal10 logic01Soal10 = new Logic01Soal10(n);

        Logic02Soal01 logic02Soal01 = new Logic02Soal01(n);
        Logic02Soal02 logic02Soal02 = new Logic02Soal02(n);
        Logic02Soal03 logic02Soal03 = new Logic02Soal03(n);
        Logic02Soal04 logic02Soal04 = new Logic02Soal04(n);
        Logic02Soal05 logic02Soal05 = new Logic02Soal05(n);
        Logic02Soal06 logic02Soal06 = new Logic02Soal06(n);
        Logic02Soal07 logic02Soal07 = new Logic02Soal07(n);
        Logic02Soal08 logic02Soal08 = new Logic02Soal08(n);
        Logic02Soal09 logic02Soal09 = new Logic02Soal09(n);
        Logic02Soal10 logic02Soal10 = new Logic02Soal10(n);

        System.out.println("logic 01 Soal 01: ");
        logic01Soal01.cetakArray();
        System.out.println("\n\nlogic 01 Soal 02: ");
        logic01Soal02.cetakArray();
        System.out.println("\n\nlogic 01 Soal 03: ");
        logic01Soal03.cetakArray();
        System.out.println("\n\nlogic 01 Soal 04: ");
        logic01Soal04.cetakArray();
        System.out.println("\n\nlogic 01 Soal 05: ");
        logic01Soal05.cetakArray();
        System.out.println("\n\nlogic 01 Soal 06: ");
        logic01Soal06.cetakArray();
        System.out.println("\n\nlogic 01 Soal 07: ");
        logic01Soal07.cetakArray();
        System.out.println("\n\nlogic 01 Soal 08: ");
        logic01Soal08.cetakArray();
        System.out.println("\n\nlogic 01 Soal 09: ");
        logic01Soal09.cetakArray();
        System.out.println("\n\nlogic 01 Soal 10: ");
        logic01Soal10.cetakArray();

        System.out.println("\n\nlogic 02 Soal 01: ");
        logic02Soal01.cetakArray();
        System.out.println("\n\nlogic 02 Soal 02: ");
        logic02Soal02.cetakArray();
        System.out.println("\n\nlogic 02 Soal 03: ");
        logic02Soal03.cetakArray();
        System.out.println("\n\nlogic 02 Soal 04: ");
        logic02Soal04.cetakArray();
        System.out.println("\n\nlogic 02 Soal 05: ");
        logic02Soal05.cetakArray();
        System.out.println("\n\nlogic 02 Soal 06: ");
        logic02Soal06.cetakArray();
        System.out.println("\n\nlogic 02 Soal 07: ");
        logic02Soal07.cetakArray();
        System.out.println("\n\nlogic 02 Soal 08: ");
        logic02Soal08.cetakArray();
        System.out.println("\n\nlogic 02 Soal 09: ");
        logic02Soal09.cetakArray();
        System.out.println("\n\nlogic 02 Soal 10: ");
        logic02Soal10.cetakArray();
    }
}
